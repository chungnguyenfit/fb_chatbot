import json, requests, random, re
from pprint import pprint

from django.views import generic
from django.http.response import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

# curl -X POST -H "Content-Type: application/json" -d '{"setting_type":"call_to_actions", "thread_state":"new_thread","call_to_actions":[{"payload":"USER_DEFINED_PAYLOAD"}]}' "https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAAERjATZCGZCEBAA2mP8GXYGZCCwFPbd6C0s9TnSfXLBRwZCZAPLVYA6BDLC22ewYKmZBP86yZAYZCqErTIZBBfijRfpsLdbaCa44VXThPtzveniCdiUFWnh7jZAKexesLdA9q0LZBltJXX4L8ZBZCjYRASb2BXc9X1ZBYKZBDrdGK4PZArKoKrpfhipHfq4"

#  ------------------------ Fill this with your page access token! -------------------------------
PAGE_ACCESS_TOKEN = "EAAERjATZCGZCEBAA2mP8GXYGZCCwFPbd6C0s9TnSfXLBRwZCZAPLVYA6BDLC22ewYKmZBP86yZAYZCqErTIZBBfijRfpsLdbaCa44VXThPtzveniCdiUFWnh7jZAKexesLdA9q0LZBltJXX4L8ZBZCjYRASb2BXc9X1ZBYKZBDrdGK4PZArKoKrpfhipHfq4"
VERIFY_TOKEN = "123456"

QA_dict = { 'how are you?': 'I am doing great.',
            'what is your name?': 'My name is Chung Nguyen.',
            'hi': 'Hi',
            'hello': 'Hello'
        }

def post_facebook_message(fbid, recevied_message, fbuser_id):
    text = recevied_message.lower()

    ret_text = ""
    if text in QA_dict:
        ret_text = QA_dict[text]
    else:
        ret_text = text
        
    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s'%PAGE_ACCESS_TOKEN
    response_msg = json.dumps({"recipient":{"id":fbid}, "message":{"text":ret_text}})
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)
    '''
    print("#### Status ####")
    pprint(status.json())
    print("################")
    '''



# Create your views here.
class CBotView(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')
        
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    def post(self, request, *args, **kwargs):
        # Converts the text payload into a python dictionary
        incoming_message = json.loads(self.request.body.decode('utf-8'))
        # Facebook recommends going through every entry since they might send
        # multiple messages in a single call during high load
        pprint(incoming_message)

    

        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                # Check to make sure the received call is a message call
                # This might be delivery, optin, postback for other events 
                if 'postback' in message:
                    fbid = message['sender']['id']
                    user_details_url = "https://graph.facebook.com/v2.6/%s"%fbid 
                    user_details_params = {'fields':'first_name', 'access_token':PAGE_ACCESS_TOKEN} 
                    user_details = requests.get(user_details_url, user_details_params).json() 
                    print("### User details:")
                    pprint(user_details)
                    ret_text = 'Hi ' + str(user_details['first_name']) +', nice to meet you.'
        
                    
                    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s'%PAGE_ACCESS_TOKEN
                    response_msg = json.dumps({"recipient":{"id":fbid}, "message":{"text":ret_text}})
                    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)

                if 'message' in message:
                    # Print the message to the terminal
                    #pprint(message)    
                    # Assuming the sender only sends text. Non-text messages like stickers, audio, pictures
                    # are sent as attachments and must be handled accordingly. 
                    
                    post_facebook_message(message['sender']['id'], message['message']['text'], message['recipient']['id'])    

        return HttpResponse()    
